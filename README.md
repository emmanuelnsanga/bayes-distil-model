# Adagrad-Genetic-Bayes-Distil-Model


References:

Bayesian optimization:
- https://findingorder.github.io/doc/bayesian.pdf
- https://www.coursera.org/learn/probabilistic-graphical-models
     

Genetic Algorithms (evolutionary): 
 - https://en.wikipedia.org/wiki/Genetic_algorithm
 - https://ocw.mit.edu/courses/mechanical-engineering/2-a35-biomimetic-principles-and-design-fall-2013/calendar-and-readings/MIT2_A35F13_genetic_algo.pdf


Knowledge distillation of model parallelism:
- https://arxiv.org/pdf/2103.12872.pdf

Adagrad optimization:
- https://wordpress.cs.vt.edu/optml/2018/03/27/adagrad/

Non-Parametric and Parametric Hybrid:
- https://influentialpoints.com/Training/nonparametric-or-parametric_bootstrap.htm

Bayes Non-Parametric:
- http://www.stat.cmu.edu/~larry/=sml/nonparbayes

