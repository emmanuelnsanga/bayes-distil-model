from bayestorch import Parameter,Data,optimizing,vb,sampling,reset
import torch
from torch.distributions import Normal


  def bayes(self):
      lambda target: bayes_params = Normal(self.params.optim).log_prob(X).sum(0), bayes_momentum = Normal(self.weight_decay), bayes_eps = Normal(self.eps)
      optimizing(target)
      print(f'optimizing: mu={mu.data}')

      res = vb(target)
      q_mu = res.params['mu']
      print(f'vb mu={q_mu["loc"]} omega={q_mu["omega"]} sigma={torch.exp(q_mu["omega"])}')

      reset()

      mu = Parameter(0.0)

      res = vb(target, q_size = 10, n_epoch=200)
      q_mu = res.params['mu']
      print(f'vb mu={q_mu["loc"]} omega={q_mu["omega"]} sigma={torch.exp(q_mu["omega"])}')

      trace = sampling(target,trace_length=300)
      mu_trace = torch.tensor([t['mu'].item() for t in trace])
      print('sampling: mu={} sigma={}'.format(torch.mean(mu_trace), torch.std(mu_trace)))
      return(trace)

