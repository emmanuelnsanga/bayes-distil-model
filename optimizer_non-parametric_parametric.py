# reference https://www.tensorflow.org/probability/api_docs/python/tfp/math/ode/Solver
# reference https://www.tensorflow.org/probability/api_docs/python/tfp/optimizer/differential_evolution_minimize

import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow_probability import distributions as tfd
import tensorflow_addons as tfa

tf.executing_eagerly()

class NonParamParamHybrid:
    def __init__(self):
        self.t_init = 0.
        self.t0 = 0.5
        self.t1 = 1.
        self.pi = tf.constant(3.14)
        self.initial_learning_rate = tf.Tensor(name='initial_learning_rate')
        self.epochs = 12  
        self.maximal_learning = tf.Tensor(name='maximal_learning_rate')
        self.sample = tf.Tensor(name='sample')
        self.step_size = tf.Tensor(name='step_size')
        self.scale_fn = tf.Tensor(name="scale_fn")
        self.DirConcen = tf.reduce_mean(self.data.get_tensor_by_name('sample')
        self.HMMParams = tf.concat(initial_distribution, transition_distribution, observation_distribution)
        self.population_params = tf.concat([self.initial_learning_rate, self.maximal_learning, self.Dir, self.HMMParams])
        self.param = tf.Tensor(name='params')
        self.data = tf.Tensor(name='sample')
        
    def population_moment(self, t, data):
        pi = self.pi
        x, y = self.data.get_tensor_by_name('sample')
        return -(tf.math.cos(x) * tf.math.cos(y) *
                 tf.math.exp(-(x-self.pi)**2 - (y-pi)**2)) #using pi in place of HMMParams

    ```
    def gradients(self, data): #acquisition function 
        results = tfp.math.ode.BDF().solve(self.population_moment, t_init, data.get_tensor_by_name('sample'),
                                        solution_times=[t0, t1])
    
        return(results.states[0])                                
    
    
    
    
    def fit_Dir(self, data): #surrogate function
        moment = population_moment(data)
        gradients = self.gradients(moment)
        DirSampleNoise = tfd.Dirichlet([self.population_params.Dir])
        DirSample = DirSampleNoise.sample([self.epochs])
        point_distance = tf.linalg.norm(DirSample - gradients, axis=-1)
        return(point_distance)

``````
  
    def update_step(self, loss_function, variables):
        lr_schedule = tfa.optimizers.CyclicalLearningRate(
        initial_learning_rate=self.population_params.get_tensor_by_name("initial_learning_rate:0"),
        maximal_learning_rate=self.population_params.get_tensor_by_name("maximal_learning_rate:0"),
        step_size=self.population_params.get_tensor_by_name("step_size"),
        scale_fn=self.population_params.get_tensor_by_name("scale_fn"),
        scale_mode='cycle',
        name = 'CyclicalLearningRate')
        optimizer = tf.optimizer.keras.optimizers.Adagrad(learning_rate=lr_schedule)
        updated_params = optimizer.minimize(loss_function)
        return(updated_params)
        
        
    
    def optimize(self):
        # The objective function and the gradient.
        gradients = self.update_step(self.fit_Dir(self.population_params), self.population_params)
        optim_results = tfp.optimizer.differential_evolution_minimize(
              gradients,
              population_params=self.population_params)
```                 
